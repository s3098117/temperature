package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    // Dummy implementation of getBookPrice method
    public double getFarenheit(String temperature) {

        double IntTemp;

        try {
            IntTemp = Integer.parseInt(temperature);
        } catch (NumberFormatException e) {
            IntTemp = 0;
        }

        return (9 * IntTemp) /5 + 32;
    }
}
